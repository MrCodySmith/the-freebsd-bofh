# The FreeBSD BOFH

Often, when starting up a new FreeBSD droplet, server, etc. I find myself doing the same tasks. I used to be able to do these via a pc-sysinstall USB, but that's become more cumbersome as projects have moved from Hardware to the Cloud. 

# Roadmap: 

- Startup scripts: Get things installed, get PKG ready, etc. 
- BOFH-Jail: Get jails created, users made up, etc. 
